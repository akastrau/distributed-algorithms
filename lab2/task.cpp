/*
 * =====================================================================================
 *
 *       Filename:  task.cpp
 *
 *    Description:  Task 1 (AR2)
 *
 *        Version:  1.10
 *        Created:  13/03/17 14:07:24
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Applied Physics and Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

// #define DEBUG // Debug mode

#include <mpi.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <string>
#include <exception>
#include <sstream>

size_t decomposeProblem(size_t countVectors, size_t &taskStartAt)
{
    const int processRank = MPI::COMM_WORLD.Get_rank();
    const int procs = MPI::COMM_WORLD.Get_size();

    size_t taskSize = countVectors / procs;

    if (processRank == 0)
    {
        taskStartAt = 0;
    }
    else
    {
        taskStartAt = processRank * (countVectors / procs);
    }

    if ((countVectors % procs != 0))
    {
        if (processRank != 0 && (processRank < (countVectors % procs)))
        {
            taskStartAt += processRank * 1;
        }
        else if (processRank != 0 && processRank >= (countVectors % procs))
        {
            taskStartAt += countVectors % procs;
        }
        if (processRank < (countVectors % procs))
        {
            taskSize += 1;
        }
    }
    return taskSize;
}

void loadDataOld(std::ifstream &file, const size_t linesCount, std::vector<std::vector<double>> &matrix)
{
    for (size_t i = 0; i != linesCount; i++)
    {
        std::string line;
        std::getline(file, line);
        std::istringstream stream(line);
        stream >> matrix[i][0] >> matrix[i][1] >> matrix[i][2];
    }
}

void loadData(MPI::File &file, const size_t howManyTasks, const size_t taskStartAt, std::vector<std::vector<double>> &matrix)
{
    char *buffer = new char[howManyTasks * 40];
    file.Read_at(40 * taskStartAt, buffer, howManyTasks * 40, MPI::CHAR);
    file.Close();

    char *floatAsBuffer = new char[11]; // 10 + 1 for '\0''

#ifdef DEBUG
    printf("Entering new mode -> LOAD DATA\n");
#endif

    for (size_t i = 0; i != howManyTasks; i++)
    {
        for (size_t j = 0; j != 3; j++)
        {
            std::copy(buffer + (i * 40) + (j * 13), buffer + (i * 40) + (j * 13) + 10, floatAsBuffer);
            matrix[i][j] = std::atof(floatAsBuffer);
        }
    }
    delete[] buffer;
    delete[] floatAsBuffer;

#ifdef DEBUG
    printf("LOADING DATA SUCCESS -> New Mode\n");
#endif
}

double calculatePartialLength(const size_t taskStartAt, size_t numberOfTasks, std::vector<std::vector<double>> matrix, bool oldMode = 1)
{
    double partialSum = 0.0f;
    if (oldMode)
    {
        for (size_t i = taskStartAt; i != (taskStartAt + numberOfTasks); i++)
        {
            double temp = 0.0f;
            for (size_t j = 0; j != 3; j++)
            {
                temp += (matrix[i][j] * matrix[i][j]);
            }
            partialSum += sqrt(temp);
        }
        return partialSum;
    }
    else
    {
        for (size_t i = 0; i != numberOfTasks; i++)
        {
            double temp = 0.0f;
            for (size_t j = 0; j != 3; j++)
            {
                temp += (matrix[i][j] * matrix[i][j]);
            }
            partialSum += sqrt(temp);
        }
        return partialSum;
    }
}

void calculatePartialAverageVector(const size_t taskStartAt, size_t numberOfTasks, std::vector<std::vector<double>> matrix, double *vector, bool oldMode = 1)
{
    if (oldMode)
    {
        for (size_t i = taskStartAt; i != (taskStartAt + numberOfTasks); i++)
        {
            for (size_t j = 0; j != 3; j++)
            {
                vector[j] += matrix[i][j];
            }
        }
    }
    else
    {
        for (size_t i = 0; i != numberOfTasks; i++)
        {
            for (size_t j = 0; j != 3; j++)
            {
                vector[j] += matrix[i][j];
            }
        }
    }
}

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);
    MPI::File newModeFile = MPI::FILE_NULL;
    MPI::Info info;
    std::ifstream file;

    double readDataTiming = MPI::Wtime();
    size_t linesCount = 0;
    bool oldMode;
    size_t taskStartAt = 0;
    size_t numberOfTasks = 0;

#ifdef DEBUG
    printf("After init...\n");
    printf("ARGC: %i\n", argc);
#endif

    if (argc > 0)
    {
        if (argc == 2)
        {
            try
            {
                // old mode
                oldMode = 1;
#ifdef DEBUG
                printf("OLD MODE reached!\n");
#endif
                file.open(argv[1]);
                if (file.fail())
                {
                    if (MPI::COMM_WORLD.Get_rank() == 0)
                    {
                        printf("Error -> file I/O failure!\n");
                    }
                    MPI::Finalize();
                    return 1;
                }
                linesCount = std::count(std::istreambuf_iterator<char>(file),
                                        std::istreambuf_iterator<char>(), '\n');
                numberOfTasks = decomposeProblem(linesCount, taskStartAt);
#ifdef DEBUG
                printf("After decomposing...\n");
#endif
            }
            catch (const std::exception &exception)
            {
                printf("Error occured: %s", exception.what());
            }
        }
        else if (argc > 1 && argv[2] == std::string("-nm"))
        {
            // new mode
            oldMode = 0;
#ifdef DEBUG
            printf("NEW MODE reached!\n");
#endif
            newModeFile = MPI::File::Open(MPI::COMM_WORLD, argv[1], MPI_MODE_RDONLY, info);
            if (newModeFile == MPI::FILE_NULL)
            {
                if (MPI::COMM_WORLD.Get_rank() == 0)
                {
                    printf("Error -> file I/O failure!\n");
                }
                MPI::Finalize();
                return 1;
            }
            linesCount = (newModeFile.Get_size() / 40);
            numberOfTasks = decomposeProblem(linesCount, taskStartAt);

#ifdef DEBUG
            printf("New Mode: lines -> %li, numberOfTasks -> %li\n", linesCount, numberOfTasks);
#endif
        }
    }
#ifdef DEBUG
    printf("Before vector init\n");
#endif
    std::vector<std::vector<double>> matrix(1, std::vector<double>(3));
#ifdef DEBUG
    printf("After vector init!\n");
#endif

    if (oldMode)
    {
        matrix.resize(linesCount);
        for (auto &iterator : matrix)
        {
            iterator.resize(3);
        }
#ifdef DEBUG
        printf("OLD MODE AFTER resize: %li\n", matrix.size());
#endif
        file.clear();
        file.seekg(0);
        loadDataOld(file, linesCount, matrix);
        file.close();
    }
    else
    {
        matrix.resize(numberOfTasks);
        for (auto &iterator : matrix)
        {
            iterator.resize(3);
        }
#ifdef DEBUG
        printf("NEW MODE AFTER resize: %li\n", matrix.size());
#endif
        loadData(newModeFile, numberOfTasks, taskStartAt, matrix);
    }

    readDataTiming = MPI::Wtime() - readDataTiming;

    double processDataTiming = MPI::Wtime();
    double partialLenghtOfVector = 0.0f;

    if (oldMode)
    {
        partialLenghtOfVector = calculatePartialLength(taskStartAt, numberOfTasks, matrix);
    }
    else
    {
        partialLenghtOfVector = calculatePartialLength(taskStartAt, numberOfTasks, matrix, 0);
    }

    double averageLengthOfVector = 0.0f;
    double partialVector[3] = {0.0f};
    double averageVector[3] = {0.0f};

    if (oldMode)
    {
        calculatePartialAverageVector(taskStartAt, numberOfTasks, matrix, partialVector);
    }
    else
    {
        calculatePartialAverageVector(taskStartAt, numberOfTasks, matrix, partialVector, 0);
    }

    processDataTiming = MPI::Wtime() - processDataTiming;

    double reduceTiming = MPI::Wtime();
    MPI::COMM_WORLD.Reduce(&partialLenghtOfVector, &averageLengthOfVector, 1, MPI::DOUBLE, MPI::SUM, 0);
    MPI::COMM_WORLD.Reduce(partialVector, averageVector, 3, MPI::DOUBLE, MPI::SUM, 0);
    reduceTiming = MPI::Wtime() - reduceTiming;

    double timingsIn[3] = {0.0f};
    timingsIn[0] = readDataTiming;
    timingsIn[1] = processDataTiming;
    timingsIn[2] = reduceTiming;

    double *timings;

    if (MPI::COMM_WORLD.Get_rank() == 0)
    {
        timings = new double[MPI::COMM_WORLD.Get_size() * 3];
    }
    MPI::COMM_WORLD.Gather(timingsIn, 3, MPI::DOUBLE, timings, 3, MPI::DOUBLE, 0);

    if (MPI::COMM_WORLD.Get_rank() == 0)
    {
        averageLengthOfVector /= linesCount;

        for (size_t i = 0; i != 3; i++)
        {
            averageVector[i] /= linesCount;
        }
        double *totalTime = new double[MPI::COMM_WORLD.Get_size()];

        for (size_t i = 0; i != MPI::COMM_WORLD.Get_size() * 3; i += 3)
        {
            totalTime[i] += timings[i] + timings[i + 1] + timings[i + 2];
        }

        std::ofstream logFile("timings.log");
        for (size_t i = 0; i != MPI::COMM_WORLD.Get_size() * 3; i += 3)
        {
            logFile << "timings (proc " << i / 3 << "):\n"
                    << "\treadData: " << timings[i]
                    << "\n\tprocessData: " << timings[i + 1] << "\n\treduceTime: " << timings[i + 2]
                    << "\n\ttotal: " << totalTime[i / 3] << "\n\n";
        }
        delete[] timings;
        delete[] totalTime;
        logFile.close();

        printf("\nStatistics from master:\n=======================\n");
        printf("Average length of vector: %f\n", averageLengthOfVector);
        printf("Average vector: (%f, %f, %f)\n", averageVector[0], averageVector[1], averageVector[2]);
        printf("Number of Vectors: %li\n", linesCount);
        oldMode ? printf("Old Mode: 1\n") : printf("Old Mode: 0\n");
    }

#ifdef DEBUG
    printf("proc %i: taskStartAt: %li, numberOfTasks: %li\n", MPI::COMM_WORLD.Get_rank(), taskStartAt, numberOfTasks);
#endif

    MPI::Finalize();
    return 0;
}