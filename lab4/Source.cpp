/*
 * =====================================================================================
 *
 *       Filename:  Source.cpp (AR4)
 *
 *    Description:  matrix multiplication - MPI C++
 *
 *        Version:  1.2
 *        Created:  06/06/2017
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Applied Physics and Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <mpi.h>

std::vector<std::vector<float>> globalA;
std::vector<std::vector<float>> globalB;
std::vector<std::vector<float>> globalC;
std::vector<std::vector<float>> globalResultMatrix;

void masterSend(std::vector<std::vector<float>> firstMatrix, std::vector<std::vector<float>> secondMatrix, int n_v, int rank, int a, int b, int q)
{
    // Sends part of matrix A and B to slaves
    int i_min = (rank / n_v) * a;
    int i_max = a + i_min;
    int j_min = (rank % n_v) * b;
    int j_max = b + j_min;

    int parameters[3] = {a, b, q}; // a, b, q

    std::string info = "[master] ";
    int packSize = MPI::INT.Pack_size(3, MPI::COMM_WORLD);
    packSize += MPI::FLOAT.Pack_size(a * q + b * q, MPI::COMM_WORLD);
    int packPosition = 0;
    char *package = new char[packSize];

    MPI::INT.Pack(&parameters[0], 3, package, packSize, packPosition, MPI::COMM_WORLD);

    for (int i = i_min; i < i_max; i++)
    {
        for (int j = 0; j < q; j++)
        {
            MPI::FLOAT.Pack(&firstMatrix[i][j], 1, package, packSize, packPosition, MPI::COMM_WORLD);
        }
    }
    for (int i = 0; i < q; i++)
    {
        for (int j = j_min; j < j_max; j++)
        {
            MPI::FLOAT.Pack(&secondMatrix[i][j], 1, package, packSize, packPosition, MPI::COMM_WORLD);
        }
    }
    std::cout << info << "Sending package to " << rank << ", size: " << packSize << " bytes" << std::endl;
    MPI::COMM_WORLD.Send(&packSize, 1, MPI::INT, rank, rank + 256);
    MPI::COMM_WORLD.Send(package, packSize, MPI::PACKED, rank, rank + 512);
    delete[] package;
}

void prepareABMatrix(std::vector<std::vector<float>> firstMatrix, std::vector<std::vector<float>> secondMatrix, int n_v, int a, int b, int q)
{
    // Prepares A, B matrix for computation
    std::string info = "[master] ";
    std::cout << info << "Preparing matrix for master process" << std::endl;
    int i_min = (0 / n_v) * a;
    int i_max = a + i_min;
    int j_min = (0 % n_v) * b;
    int j_max = b + j_min;

    globalA.resize(a);
    globalB.resize(q);
    for (int i = i_min; i < i_max; i++)
    {
        for (int j = 0; j < q; j++)
        {
            globalA[i].push_back(firstMatrix[i][j]);
        }
    }
    for (int i = 0; i < q; i++)
    {
        for (int j = j_min; j < j_max; j++)
        {
            globalB[i].push_back(secondMatrix[i][j]);
        }
    }
}

void masterReceive(int rank, int a, int b, int n_v)
{
    // Receives partial results from slaves
    int i_min = (rank / n_v) * a;
    int i_max = a + i_min;
    int j_min = (rank % n_v) * b;
    int j_max = b + j_min;

    int packSize = 0;
    int packPosition = 0;
    std::string info = "[master] ";

    MPI::COMM_WORLD.Recv(&packSize, 1, MPI::INT, rank, rank + 1024);
    char *package = new char[packSize];
    MPI::COMM_WORLD.Recv(package, packSize, MPI::PACKED, rank, rank + 1280);

    std::cout << info << "Received package from process " << rank << std::endl;
    // Unpacking
    for (int i = i_min; i < i_max; i++)
    {
        for (int j = j_min; j < j_max; j++)
        {
            MPI::FLOAT.Unpack(package, packSize, &globalResultMatrix[i][j], 1, packPosition, MPI::COMM_WORLD);
        }
    }
    delete[] package;
}

void copyCMatrixToResultMatrix(int a, int b)
{
    std::string info = "[master] ";
    std::cout << info << "Copying small C matrix to master's result matrix" << std::endl;
    for (int i = 0; i < a; i++)
    {
        for (int j = 0, l = 0; j < b; j++)
        {
            globalResultMatrix[i][j] = globalC[i][j];
        }
    }
}

void slaveSend(int rank)
{
    // Send partial result to master
    int a = globalC.size();
    int b = globalC[0].size();

    int packSize = MPI::FLOAT.Pack_size(a * b, MPI::COMM_WORLD);
    int packPosition = 0;
    char *package = new char[packSize];

    for (int i = 0; i < a; i++)
    {
        for (int j = 0; j < b; j++)
        {
            MPI::FLOAT.Pack(&globalC[i][j], 1, package, packSize, packPosition, MPI::COMM_WORLD);
        }
    }
    MPI::COMM_WORLD.Send(&packSize, 1, MPI::INT, 0, rank + 1024);
    MPI::COMM_WORLD.Send(package, packSize, MPI::PACKED, 0, rank + 1280);
    delete[] package;
}

void slaveReceive(int rank)
{
    // Retrieve A and B matrix from master
    int packSize = 0;
    int packPosition = 0;
    MPI::COMM_WORLD.Recv(&packSize, 1, MPI::INT, 0, rank + 256);
    char *package = new char[packSize];
    MPI::COMM_WORLD.Recv(package, packSize, MPI::PACKED, 0, rank + 512);

    int parameters[3] = {0};
    MPI::INT.Unpack(package, packSize, &parameters[0], 3, packPosition, MPI::COMM_WORLD);

    globalA.resize(parameters[0]);
    for (int i = 0; i < parameters[0]; i++)
    {
        globalA[i].resize(parameters[2]);
    }
    globalB.resize(parameters[2]);
    for (int i = 0; i < parameters[2]; i++)
    {
        globalB[i].resize(parameters[1]);
    }

    for (int i = 0; i < parameters[0]; i++)
    {
        for (int j = 0; j < parameters[2]; j++)
        {
            MPI::FLOAT.Unpack(package, packSize, &globalA[i][j], 1, packPosition, MPI::COMM_WORLD);
        }
    }
    for (int i = 0; i < parameters[2]; i++)
    {
        for (int j = 0; j < parameters[1]; j++)
        {
            MPI::FLOAT.Unpack(package, packSize, &globalB[i][j], 1, packPosition, MPI::COMM_WORLD);
        }
    }
    delete[] package;
}

void multiply()
{
    // Multiply global matrix A and B. Results are stored in globalC matrix
    globalC.resize(globalA.size());
    for (int i = 0; i < globalA.size(); i++)
    {
        globalC[i].resize(globalB[0].size());
    }

    for (int i = 0; i < globalC.size(); i++)
    {
        for (int j = 0; j < globalC[0].size(); j++)
        {
            for (int k = 0; k < globalB.size(); k++)
            {
                globalC[i][j] += globalA[i][k] * globalB[k][j];
            }
        }
    }
}

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);
    double calculationTime, sendTime, recvTime, totalCalculationTime,
        totalRecvTime, totalSendTime;
    if (argc < 4)
    {
        MPI::Finalize();
        return 1;
    }

    if (MPI::COMM_WORLD.Get_rank() == 0)
    {
        std::string inFileA(argv[1]);
        std::string inFileB(argv[2]);
        std::string outFile(argv[3]);

        std::string info = "[master] ";

        int p = 0, q = 0, m = 0, r = 0; // p x q = dim A, m x r = dim B
        double readTime, writeTime, decompositionTime;

        std::cout << info << "Process started" << std::endl;

        std::ifstream fileA(inFileA);
        fileA >> p >> q;

        std::ifstream fileB(inFileB);
        fileB >> m >> r;

        if (q != m)
        {
            std::cout << info << "Invalid shape!" << std::endl;
            fileA.close();
            fileB.close();
            MPI::COMM_WORLD.Abort(1);
        }

        int n_h, n_v = 0, a = 0, b = 0;

        decompositionTime = MPI::Wtime();

        for (n_h = 1; n_h <= MPI::COMM_WORLD.Get_size(); n_h++)
        {
            int n = MPI::COMM_WORLD.Get_size();
            n_v = n / n_h;
            if (n % n_h == 0 && p % n_h == 0 && r % n_v == 0)
            {
                a = p / n_h;
                b = r / n_v;
                break;
            }
        }

        if (n_v == 0)
        {
            std::cout << info << "We couldn't calculate a, b, n_h, n_v!" << std::endl;
            MPI::COMM_WORLD.Abort(1);
        }
        decompositionTime = MPI::Wtime() - decompositionTime;

        std::cout << info << "Parameters = [n_v = " << n_v << ", n_h = " << n_h
                  << ", a = " << a << ", b = " << b << ", p = " << p << ", q = "
                  << q << ", r = " << r << "]" << std::endl;
        std::cout << info << "Total size of data: "
                  << MPI::COMM_WORLD.Get_size() * (3 * sizeof(int) + (a * q + q * b) * sizeof(float))
                  << " bytes" << std::endl;

        std::vector<std::vector<float>> matrixA(p, std::vector<float>(q));
        std::vector<std::vector<float>> matrixB(m, std::vector<float>(r));

        readTime = MPI::Wtime();

        std::cout << info << "Reading files..." << std::endl;
        for (int i = 0; i < p; i++)
        {
            for (int j = 0; j < q; j++)
            {
                fileA >> matrixA[i][j];
            }
        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < r; j++)
            {
                fileB >> matrixB[i][j];
            }
        }
        fileA.close();
        fileB.close();
        readTime = MPI::Wtime() - readTime;

        std::cout << info << "...done" << std::endl;

        globalResultMatrix.resize(p);
        for (int i = 0; i < p; i++)
        {
            globalResultMatrix[i].resize(r);
        }

        sendTime = MPI::Wtime();
        prepareABMatrix(matrixA, matrixB, n_v, a, b, q);
        for (int i = 1; i < MPI::COMM_WORLD.Get_size(); i++)
        {
            masterSend(matrixA, matrixB, n_v, i, a, b, q);
        }
        sendTime = MPI::Wtime() - sendTime;
        matrixA.clear();
        matrixB.clear();
        matrixA.shrink_to_fit();
        matrixB.shrink_to_fit();

        calculationTime = MPI::Wtime();
        multiply();
        calculationTime = MPI::Wtime() - calculationTime;

        recvTime = MPI::Wtime();
        copyCMatrixToResultMatrix(a, b);

        for (int i = 1; i < MPI::COMM_WORLD.Get_size(); i++)
        {
            masterReceive(i, a, b, n_v);
        }
        recvTime = MPI::Wtime() - recvTime;

        std::cout << info << "Saving file..." << std::endl;
        writeTime = MPI::Wtime();
        std::ofstream fileC(outFile);

        fileC << p << " " << q << std::endl;

        for (int i = 0; i < p; i++)
        {
            for (int j = 0; j < r; j++)
            {
                fileC << globalResultMatrix[i][j] << " ";
            }
            fileC << std::endl;
        }
        fileC.close();
        writeTime = MPI::Wtime() - writeTime;
        std::cout << info << "...done" << std::endl;

        MPI::COMM_WORLD.Reduce(&calculationTime, &totalCalculationTime, 1, MPI::DOUBLE, MPI::SUM, 0);
        MPI::COMM_WORLD.Reduce(&recvTime, &totalRecvTime, 1, MPI::DOUBLE, MPI::SUM, 0);
        MPI::COMM_WORLD.Reduce(&sendTime, &totalSendTime, 1, MPI::DOUBLE, MPI::SUM, 0);

        std::cout << info << "MASTER TIMES: " << std::endl;
        std::cout << info << "read time: " << readTime << " decomposition time: "
                  << decompositionTime << std::endl
                  << info << "send time: " << sendTime << " calculation time: "
                  << calculationTime << " receive time: " << recvTime << std::endl
                  << info << "write time: " << writeTime << std::endl;

        std::cout << info << "TOTAL TIMES: " << std::endl;
        std::cout << info << "send time: " << totalSendTime << " calculation time: "
                  << totalCalculationTime << " receive time: " << totalRecvTime << std::endl;
    }
    else
    {
        recvTime = MPI::Wtime();
        slaveReceive(MPI::COMM_WORLD.Get_rank());
        recvTime = MPI::Wtime() - recvTime;

        calculationTime = MPI::Wtime();
        multiply();
        calculationTime = MPI::Wtime() - calculationTime;

        sendTime = MPI::Wtime();
        slaveSend(MPI::COMM_WORLD.Get_rank());
        sendTime = MPI::Wtime() - sendTime;

        MPI::COMM_WORLD.Reduce(&calculationTime, &totalCalculationTime, 1, MPI::DOUBLE, MPI::SUM, 0);
        MPI::COMM_WORLD.Reduce(&recvTime, &totalRecvTime, 1, MPI::DOUBLE, MPI::SUM, 0);
        MPI::COMM_WORLD.Reduce(&sendTime, &totalSendTime, 1, MPI::DOUBLE, MPI::SUM, 0);
    }

    MPI::Finalize();
    return 0;
}