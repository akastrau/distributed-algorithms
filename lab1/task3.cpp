/*
 * =====================================================================================
 *
 *       Filename:  task3.cpp
 *
 *    Description:  Task 3 (AR1)
 *
 *        Version:  1.0
 *        Created:  03/03/17 16:30:58
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Applied Physics and  Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <ctime>
#include <memory>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>

int main(int argc, char* argv[]){
    MPI::Init(argc, argv);
    const int rank = MPI::COMM_WORLD.Get_rank();
    const int procs = MPI::COMM_WORLD.Get_size();
    MPI::Status status;

    const int packSize = MPI::FLOAT.Pack_size(procs * procs, MPI::COMM_WORLD);
    char* packBufferToSend = new char[packSize];
    char* packBufferToReceive = new char[packSize];
    int packPosition = 0;

    std::vector<std::vector<float>> matrix(procs, std::vector<float>(procs));
    std::vector<std::vector<float>> matrixReceived(procs, std::vector<float>(procs));

    int destination = -1;
    srand((unsigned)time(NULL) + rank + 1 * procs);
    for (int i = 0; i < procs; i++){
        for (int j = 0; j < procs; j++){
            matrix[i][j] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
            MPI::FLOAT.Pack(&matrix[i][j], 1, packBufferToSend, packSize, packPosition, MPI::COMM_WORLD);
        }
    }
    packPosition = 0;
    if (rank % 2 == 0){
        destination = rank + 1;
        MPI::COMM_WORLD.Send(packBufferToSend, packSize, MPI::PACKED, destination, rank + 1 + destination + 1);
        delete[] packBufferToSend;
        printf("[proc %i]: Sent to %i -> TAG = %i\n", rank, destination, rank + 1 + destination + 1);
        printf("[proc %i]: Waiting for %i -> TAG = %i\n", rank, destination, rank + 1 + destination - 1);
        MPI::COMM_WORLD.Recv(packBufferToReceive, packSize, MPI::PACKED, destination, rank + 1 + destination - 1, status);
        printf("[proc %i]: Received from %i -> TAG = %i\n", rank, destination, rank + 1 + destination - 1);
        float matrixElement;
        for (int i = 0; i < procs; i++){
            for (int j = 0; j < procs; j++){
                MPI::FLOAT.Unpack(packBufferToReceive, packSize, &matrixElement, 1, packPosition, MPI::COMM_WORLD);
                matrixReceived[i][j] = matrixElement;
            }
        }
        delete[] packBufferToReceive;
    } else {
        destination = rank - 1;
        MPI::COMM_WORLD.Send(packBufferToSend, packSize, MPI::PACKED, destination, rank + 1 + destination - 1);
        delete[] packBufferToSend;
        printf("[proc %i]: Sent to %i -> TAG = %i\n", rank, destination, rank + 1 + destination - 1);
        printf("[proc %i]: Waiting for %i -> TAG = %i\n", rank, destination, rank + 1 + destination + 1);
        MPI::COMM_WORLD.Recv(packBufferToReceive, packSize, MPI::PACKED, destination, rank + 1 + destination + 1, status);
        printf("[proc %i]: Received from %i -> TAG = %i\n", rank, destination, rank + 1 + destination + 1);
        float matrixElement;
        for (int i = 0; i < procs; i++){
            for (int j = 0; j < procs; j++){
                MPI::FLOAT.Unpack(packBufferToReceive, packSize, &matrixElement, 1, packPosition, MPI::COMM_WORLD);
                matrixReceived[i][j] = matrixElement;
            }
        }
        delete[] packBufferToReceive;
    }

    std::ostringstream fileName;
    fileName << "log.proc_0" << rank;
    std::ofstream logFile;
    logFile.open((fileName.str()).c_str());
    logFile << "Generated matrix: " << std::endl;

    for (int i = 0; i < procs; i++){
        for (int j = 0; j < procs; j++){
           logFile << matrix[i][j] << " ";
        }
        logFile << std::endl;
    }
    logFile << std::endl << "Received matrix: " << std::endl;
      for (int i = 0; i < procs; i++){
        for (int j = 0; j < procs; j++){
           logFile << matrixReceived[i][j] << " ";
        }
        logFile << std::endl;
    }
    logFile << std::endl << "Communication with: " << destination;
    logFile.close();

    matrix.clear();
    matrixReceived.clear();
    matrix.shrink_to_fit();
    matrixReceived.shrink_to_fit();

    MPI::Finalize();
    return 0;
}
