/*
 * =====================================================================================
 *
 *       Filename:  task1.cpp
 *
 *    Description:  Task 1
 *
 *        Version:  1.0
 *        Created:  03/02/2017 08:19:41 AM
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Technical Physics and Applied Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

 #include <iostream>
 #include "mpi.h"

int main(int argc, char* argv[]){

    MPI_Init(&argc, &argv);

    int rank = 0, procs = 0; // procs - ilosc procesow, rank - numer obecnego procesu
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);

    std::cout << "Hello World! I’m process " << rank << " and there are " << procs <<
        " processes in total!" << std::endl << std::flush;

    MPI_Finalize();
    return 0;
}

