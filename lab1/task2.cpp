/*
 * =====================================================================================
 *
 *       Filename:  task2.cpp
 *
 *    Description:  Task 2 (AR1)§
 *
 *        Version:  1.0
 *        Created:  03/02/2017 08:32:38 AM
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Technical Physics and Applied Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */


#include <iostream>
#include "mpi.h"
#include <cstdlib>
#include <cstdio>
#include <ctime>

 int main(int argc, char** argv){
    MPI::Init(argc, argv);
    MPI::Status status;
	int rank = MPI::COMM_WORLD.Get_rank();
	int procs = MPI::COMM_WORLD.Get_size();

	if (rank != 0){
		srand((unsigned)time(NULL) + rank + 1 * procs + 0xfff);
		int data = rand() % procs + 1;
		printf("[slave %i]: sent to master -> %i\n", rank, data);
		MPI::COMM_WORLD.Send(&data, 1, MPI::INT, 0, rank); // wyslij o tagu rownym rank
		MPI::COMM_WORLD.Recv(&data, 1, MPI::INT, 0, MPI::ANY_TAG, status);
		printf("[slave %i]: received from master -> %i\n", rank, data);
	} else {
		int data = 0;
		int sum = 0;
		for (int i = 1; i < procs; i++){
			MPI::COMM_WORLD.Recv(&data, 1, MPI::INT, MPI::ANY_SOURCE, MPI::ANY_TAG, status);
			printf("[master]: received number from slave %i -> %i\n", status.Get_source(), data);
			sum += data;
		}
		for (int i = 1; i < procs; i++){
			MPI::COMM_WORLD.Send(&sum, 1, MPI::INT, i, i + 10);
			printf("[master]: sent to slave %i -> %i\n", i , sum);
		}
	}

    MPI::Finalize();
    return 0;
 }
