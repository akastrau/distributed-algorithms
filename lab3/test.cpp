#include <mpi.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

int pack_size = 0;     // Rozmiar paczki do wyslania
int pack_position = 0; // Pozycja podczas pakowania (korzystaja z tego funkcje Pack i Unpack zeby wiedziec w jakim miejscu skonczyly pakowania/rozpakowywanie)
char *pack_buffer;     // Bufor na paczke
int slave_degree[2];   // tablica gdzie masz stopien wielomianu - pozycja 0; poz. 1 - integration czyli liczba podpodzialow do calkowania
float *slave_floats;   // wspolczynniki wielomianu + granice calkowania

double evaluate_f_of_x(double x)
{
    double sum = 0;
    for (int i = 0; i <= slave_degree[0]; i++)
    {
        sum += slave_floats[i] * pow(x, i);
    }
    return sum;
} // funkcja obliczajaca wartosc funkcji w punkcie x
double integrate(double a, double b, int m)
{
    double x;
    double f_of_x;
    double h = (b - a) / m;
    double integral = 0.0;
    for (int i = 0; i <= m; i++)
    {
        x = a + i * h;
        f_of_x = evaluate_f_of_x(x);
        if ((i == 0) || (i == m))
            integral += 0.5 * f_of_x;
        else
            integral += f_of_x;
    }
    integral *= h;
    return integral;
} // Funkcja calkujaca z pliku pdf

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);

    int rank = MPI::COMM_WORLD.Get_rank(); // Ranga procesu
    int size = MPI::COMM_WORLD.Get_size(); // Ilosc procesow

    if (rank == 0)
    {
        ifstream file(argv[1]); // Otwieranie pliku na masterze - nazwa podana jako argument po nazwie pliku wykonywalnego
        int degreeI[2];         // tymczasowe tablice (analogiczne do tablic globalnych)
        float *float_values;
        int counter = 0; // licznik linii
        // string line;

        while (file.seekg(12, file.cur)) // Przesuwamy sie o 12 znakow wzgledem obecnej pozycji kursora w pliku
        {
            // UWAGA! Zakomentowalem linie, bo mozna to jeszcze uproscic, tzn. nie uzywajac zamiany stringa na strumien

            // getline(file, line); // W ten sposob omijamy slowa, wczytujemy reszte czyli inty lub floaty
            // istringstream stream(line); // zamiana stringa na linie
            if (counter == 0)
            {
                // stream >> degreeI[0];
                file >> degreeI[0];
                float_values = new float[degreeI[0] + 3]; // rezerwujemy pamiec dla floatow degreeI[0] znaczy stopien wielomianu
                                                          // Wspolczyniikow zawsze bedzie stopien + 1, ale chcemy miec tez 2 przedzialy calowania wiec + 2 czyli stopien + 3
            }
            else if (counter < 3)
            {
                for (int i = 0; i < degreeI[0] + 1; i++)
                {
                    file >> float_values[i];
                    // stream >> float_values[i];
                }
                file.seekg(12, file.cur);
                file >> float_values[degreeI[0] + 1] >> float_values[degreeI[0] + 2]; // wczytujemy przedzialy calkowania - zawsze na koncu tablicy z floatami
                // std::getline(file, line);
                // istringstream stream2(line);
                // stream2 >> float_values[degreeI[0] + 1] >> float_values[degreeI[0] + 2];
                counter++;
            }
            else
            {
                file >> degreeI[1]; // Wczytujemy integration
                                    // stream >> degreeI[1];
            }

            counter++;
        }
        cout << degreeI[0] << " ";
        for (int i = 0; i < degreeI[0] + 3; i++)
        {
            cout << float_values[i] << " ";
        }
        cout << degreeI[1] << endl;
        // Ustalanie rozmiaru paczki; zawsze 2 floaty i dynamicznie w zaleznosci od  stopnia wielomianu liczba floatow
        pack_size = MPI::INT.Pack_size(2, MPI::COMM_WORLD);
        pack_size += MPI::FLOAT.Pack_size(degreeI[0] + 3, MPI::COMM_WORLD);

        // Alokacja pamieci dla paczki
        pack_buffer = new char[pack_size];
        // Pakowanie inty potem floaty
        MPI::INT.Pack(&degreeI[0], 2, pack_buffer, pack_size, pack_position, MPI::COMM_WORLD);
        MPI::FLOAT.Pack(&float_values[0], degreeI[0] + 3, pack_buffer, pack_size, pack_position, MPI::COMM_WORLD);

        pack_position = 0;

        delete[] float_values;

        // wysylanie paczki do innych procesow
        MPI::COMM_WORLD.Bcast(&pack_size, 1, MPI::INT, 0);
        MPI::COMM_WORLD.Bcast(pack_buffer, pack_size, MPI::PACKED, 0);
    }

    if (rank != 0)
    {
        // na innych procesach, odbieram rozmiar paczki
        MPI::COMM_WORLD.Bcast(&pack_size, 1, MPI::INT, 0);
        // alokuje pamiec dla paczki
        pack_buffer = new char[pack_size];
        // Odbieram paczke
        MPI::COMM_WORLD.Bcast(pack_buffer, pack_size, MPI::PACKED, 0);
    }
    // Rozpakowywanie intow
    MPI::INT.Unpack(pack_buffer, pack_size, &slave_degree[0], 2, pack_position, MPI::COMM_WORLD);
    // Alokacja pamieci dla tablicy globalnej
    slave_floats = new float[slave_degree[0] + 3];
    // Rozpakowywanie
    MPI::FLOAT.Unpack(pack_buffer, pack_size, &slave_floats[0], slave_degree[0] + 3, pack_position, MPI::COMM_WORLD);
    cout << "coef: " << slave_floats[0] << endl;

    // Dekompozycja z pliku pdfa
    int p = slave_degree[1] / size;
    int r = slave_degree[1] % size;
    int my_number_of_subintervals;
    int my_first_midpoint;
    if (rank < r)
    {
        my_number_of_subintervals = p + 1;
        my_first_midpoint = rank * (p + 1);
    }
    else
    {
        my_number_of_subintervals = p;
        my_first_midpoint = r * (p + 1) + (rank - r) * p;
    }
    // Ustalanie indywidualnych przedzialow calkowania dla danych procesow na podstawie dekompozycji
    // interval_a - dolna granica calkowania, interval_b - dolna; slave_degree[1] - liczba podpodzialow
    float interval_a = slave_floats[slave_degree[0] + 1];
    float interval_b = slave_floats[slave_degree[0] + 2];

    // Obliczanie przedzialow calkowania dla procesu
    double a = interval_a + ((interval_b - interval_a) / slave_degree[1] * my_first_midpoint);
    double b = interval_a + ((interval_b - interval_a) / slave_degree[1] * (my_first_midpoint + my_number_of_subintervals));

    // Calkowanie
    double integration_result = integrate(a, b, my_number_of_subintervals);

    double result = 0;
    // Zbieranie wynikow poprzez sumowanie
    MPI::COMM_WORLD.Reduce(&integration_result, &result, 1, MPI::DOUBLE, MPI::SUM, 0);

    if (rank == 0)
    {
        double integral_a = 0, integral_b = 0;
        // Obliczanie calki w sposob analityczny na podstawie wzoru z pdfa
        for (int i = 0; i <= slave_degree[0]; i++)
        {
            integral_a += slave_floats[i] * pow(slave_floats[slave_degree[0] + 1], i + 1) / (i + 1);
            integral_b += slave_floats[i] * pow(slave_floats[slave_degree[0] + 2], i + 1) / (i + 1);
        }
        // Wyswietlanie wyniku numerycznego i analitycznego
        cout << "Result: " << result << endl;
        cout << "Results_1: " << integral_b - integral_a << endl;
    }
    // Dealokacja pamieci
    delete[] slave_floats;
    MPI::Finalize();
    return 0;
}