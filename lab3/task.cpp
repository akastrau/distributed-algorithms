/*
 * =====================================================================================
 *
 *       Filename:  task.cpp
 *
 *    Description:  Task 1 (AR3)
 *
 *        Version:  1.0
 *        Created:  13/03/17 11:29:24
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Applied Physics and Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

// #define DEBUG // Debug mode

#include <mpi.h>
<<<<<<< HEAD
=======
#include <cmath>
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>

double evaluate_f_of_x(double x, float *data, int dataSize)
{
<<<<<<< HEAD
    #ifdef DEBUG
=======
#ifdef DEBUG
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9
    if (MPI::COMM_WORLD.Get_rank() == 1)
    {
        printf("evaluating... ");
        for (int i = 0; i < dataSize + 1; i++)
        {
            printf("%f ", data[i]);
        }
        printf("eval\n");
    }
#endif
    double result = 0;

    for (int i = 0; i < dataSize + 1; i++)
    {
        result += data[i] * pow(x, i);
    }
    return result;
}

double integrate(double a, double b, int m, float *data, int dataSize)
{
    double x;
    double f_of_x;
    double h = (b - a) / m;
    double integral = 0.0;
    for (int i = 0; i <= m; i++)
    {
        x = a + i * h;
        f_of_x = evaluate_f_of_x(x, data, dataSize);
        if ((i == 0) || (i == m))
            integral += 0.5 * f_of_x;
        else
            integral += f_of_x;
    }
    integral *= h;
    return integral;
}

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);
    std::string args(argv[1]);

    if (argc != 2 && args != "")
    {
        MPI::Finalize();
        return 1;
    }

    char *bufferToSend;
    float *coeffsAndInterval;
    int *degreeAndIntegration = new int[2];
    int packPosition = 0;
    int packSize = 0;

    if (MPI::COMM_WORLD.Get_rank() == 0)
    {
        std::ifstream file(argv[1]);
        std::vector<int> integers(2);

<<<<<<< HEAD

=======
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9
        file.seekg(7, std::ios::cur);
        file >> integers[0]; // integers[0] = degree
        file.seekg(7, std::ios::cur);
        std::vector<float> floats(integers[0] + 3); // coeffs = degree + 1, intervals = degree += 2
        for (int i = 0; i < integers[0] + 1; i++)
        {
            file >> floats[i];
        }
        file.seekg(9, std::ios::cur);
        file >> floats[integers[0] + 1] >> floats[integers[0] + 2];
        file.seekg(12, std::ios::cur);
        file >> integers[1]; // integration
        file.close();

#ifdef DEBUG
        printf("%i\n", integers[0]);
#endif

#ifdef DEBUG
        for (auto v : floats)
        {
            printf("%f\n", v);
        }
#endif
        packSize = MPI::INT.Pack_size(2, MPI::COMM_WORLD);
        packSize += MPI::FLOAT.Pack_size(integers[0] + 3, MPI::COMM_WORLD);
        bufferToSend = new char[packSize];

        MPI::INT.Pack(&integers[0], 1, bufferToSend, packSize, packPosition, MPI::COMM_WORLD);
        MPI::FLOAT.Pack(&floats[0], integers[0] + 3, bufferToSend, packSize, packPosition, MPI::COMM_WORLD);
        MPI::INT.Pack(&integers[1], 1, bufferToSend, packSize, packPosition, MPI::COMM_WORLD);

<<<<<<< HEAD
        #ifdef DEBUG
        printf("Pack end reached!\n");
        #endif
=======
#ifdef DEBUG
        printf("Pack end reached!\n");
#endif
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9
    }
    packPosition = 0;
    MPI::COMM_WORLD.Bcast(&packSize, 1, MPI::INT, 0);

    if (MPI::COMM_WORLD.Get_rank() != 0)
    {
        bufferToSend = new char[packSize];
    }
    MPI::COMM_WORLD.Bcast(bufferToSend, packSize, MPI::PACKED, 0);

<<<<<<< HEAD
      #ifdef DEBUG
        printf("Bcast end reached!\n");
        #endif
=======
#ifdef DEBUG
    printf("Bcast end reached!\n");
#endif
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9

    MPI::INT.Unpack(bufferToSend, packSize, &degreeAndIntegration[0], 1, packPosition, MPI::COMM_WORLD);
    coeffsAndInterval = new float[degreeAndIntegration[0] + 3];
    MPI::FLOAT.Unpack(bufferToSend, packSize, &coeffsAndInterval[0], degreeAndIntegration[0] + 3, packPosition, MPI::COMM_WORLD);
    MPI::INT.Unpack(bufferToSend, packSize, &degreeAndIntegration[1], 1, packPosition, MPI::COMM_WORLD);

    delete[] bufferToSend;

<<<<<<< HEAD
 #ifdef DEBUG
        printf("Unpack end reached!\n");
        #endif
=======
#ifdef DEBUG
    printf("Unpack end reached!\n");
#endif
>>>>>>> 83e9a6ac9bf23ffd20db1a823228e2d48632ffa9

#ifdef DEBUG
    if (MPI::COMM_WORLD.Get_rank() == 1)
    {
        for (int i = 0; i < degreeAndIntegration[0] + 3; i++)
        {
            printf("%f\n", coeffsAndInterval[i]);
        }
    }
#endif

    int p = degreeAndIntegration[1] / MPI::COMM_WORLD.Get_size();
    int r = degreeAndIntegration[1] % MPI::COMM_WORLD.Get_size();
    int my_number_of_subintervals;
    int my_first_midpoint;
    if (MPI::COMM_WORLD.Get_rank() < r)
    {
        my_number_of_subintervals = p + 1;
        my_first_midpoint = MPI::COMM_WORLD.Get_rank() * (p + 1);
    }
    else
    {
        my_number_of_subintervals = p;
        my_first_midpoint = r * (p + 1) + (MPI::COMM_WORLD.Get_rank() - r) * p;
    }
    float intervalStart = coeffsAndInterval[degreeAndIntegration[0] + 1];
    float intervalEnd = coeffsAndInterval[degreeAndIntegration[0] + 2];
    int integration = degreeAndIntegration[1];

    double a = intervalStart + ((intervalEnd - intervalStart) / integration * (my_first_midpoint));
    double b = intervalStart + ((intervalEnd - intervalStart) / integration * (my_first_midpoint + my_number_of_subintervals));

#ifdef DEBUG
    printf("%lf %lf %i %i\n", a, b, my_first_midpoint, my_number_of_subintervals);
#endif

    double partialResult = integrate(a,
                                     b,
                                     my_number_of_subintervals, coeffsAndInterval, degreeAndIntegration[0]);
    double result = 0;
    MPI::COMM_WORLD.Reduce(&partialResult, &result, 1, MPI::DOUBLE, MPI::SUM, 0);

    if (MPI::COMM_WORLD.Get_rank() == 0)
    {
        double integralB = 0;
        double integralA = 0;

        for (int i = 0; i <= degreeAndIntegration[0]; i++)
        {
            integralB += (coeffsAndInterval[i] * pow(intervalEnd, i + 1)) / (i + 1);
        }

        for (int i = 0; i <= degreeAndIntegration[0]; i++)
        {
            integralA += (coeffsAndInterval[i] * pow(intervalStart, i + 1)) / (i + 1);
        }

        printf("Result: %.17lf\n", result);
        printf("Analytical result: %.17lf\n", integralB - integralA);
    }

    delete[] degreeAndIntegration;
    delete[] coeffsAndInterval;

    MPI::Finalize();
    return 0;
}