/*
 * =====================================================================================
 *
 *       Filename:  template.cpp
 *
 *    Description:  Template for MPI projects
 *
 *        Version:  1.0
 *        Created:  03/02/2017 08:11:34 AM
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Adrian Kastrau (Faculty of Technical Physics and Applied Mathematics), adrkastr@student.pg.gda.pl
 *   Organization:  Gdansk University of Technology
 *
 * =====================================================================================
 */

#include <iostream>
#include "mpi.h"

int main(int argc, char* argv[]){

    MPI_Init(&argc, &argv);


    MPI_Finalize();
    return 0;
}
